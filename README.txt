CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Use drupal as external auth backend for apache and other webservers.
Apache example included.
ALWAYS USE SSL FOR THIS AS PASSWORDS ARE SENT OVER THE WIRE!


REQUIREMENTS
------------

No depencies on contrib modules.


INSTALLATION
------------

 * Install this module (caa).
 * Configure a safe API-key on admin/caa/external-auth-settings.


CONFIGURATION
-------------

Based on Ubuntu 14.04 LTS, assuming you're using Apache2:
-> sudo apt-get install libapache2-mod-authnz-external

-> Include example at https://www.drupal.org/sandbox/reinier-vegter/2424123
   in the VHOST you'd like to protect.
   Be aware that the script-path must be absolute.

-> Make sure sites/all/modules/custom/caa/scripts/caa-apache.php is EXECUTABLE
   $ chmod +x [file]
   If risk of resetting filemode is too high, copy the file outside the
   webroot, and point DefineExternalAuth (vhost) to it.
-> Visit /admin/config/people/caa and configure an API-Key.
-> Configure caa-apache.php (edit the file).

Notes:
 - The php-script caa-apache.php does not allow for redirects because of
   security issues.
   For the ldap_sso module this is fixed by adding the api path to excludes,
   but other modules or server-configs might interfere.
 - The php-script keeps it's own secure cache in the system temp folder.
   Sessions stay alive for 5 minutes, and are saved in hashed files, which
   cannot be resolved to usernames or passwords.
   Old session files are automatically removed during requests.
 - Be aware that passwords are sent plain-text.
   ALWAYS MAKE SURE TO RUN YOUR DRUPAL INSTANCE BEHIND SSL.
 - SSL: if using SSL (Drupal url starts with https://), only valid
   certificates are allowed!
   Self-signed certificates won't work, as it is not secure in any
   context running Drupal.
 - If using a SSL3 certificate on the webhost, you should use the curl method
   by configuring in the script.

mod-authnz package name for other distributions:
 - Ubuntu 12.04 LTS: libapache2-mod-authnz-external
 - Debian 7: libapache2-mod-authnz-external

Compile on your own (CentOS, RHEL etc)?
See https://code.google.com/p/mod-auth-external/wiki/Installation .


MAINTAINERS
-----------

Current maintainers:
 * Reinier Vegter (reinier-V) - https://www.drupal.org/u/reinier-v
