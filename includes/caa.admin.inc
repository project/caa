<?php
/**
 * @file
 * Include file with admin functions.
 *
 * Functions:
 * - External auth backend api key.
 * - Debug mode.
 */

/**
 * Helper function: Settings form for API key.
 */
function caa_admin() {
  $form = array();
  $form['caa_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key for auth backend'),
    '#default_value' => variable_get('caa_api_key', ''),
    '#required' => FALSE,
  );
  $form['caa_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug logging to dblog'),
    '#default_value' => variable_get('caa_debug', FALSE),
    '#required' => FALSE,
  );
  return system_settings_form($form);
}
