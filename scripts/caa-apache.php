#!/usr/bin/env php

<?php
/**
 * @file
 * Apache mod-authnz-external auth backend.
 *
 * This file is a standalone script which provides an authentication
 * backend for apache through apache module mod-authnz-external.
 *
 * Features:
 *  - 5 minute (secure) session cache to remove normal user load from backend.
 *  - Not allowing any redirects.
 *  - If using SSL (which you should!), only allow for valid certificates.
 *  - Configure SSL version.
 *  - Curl debug output.
 *  - Add extra CA-bundle to trust.
 *
 * Test this from shell with command like:
 *   echo -e "USERNAME\nPASSWORD" | ./caa-apache.php https://drupal.mydomain.com
 *   Mind the "\n" in the echo, it provides a newline needed for this script!
 */

/**
 * Configuration parameters.
 *
 * CAA_API_KEY: API-key for Drupal backend.
 * CAA_SSL_V: SSL version to use.
 *   If 0, it's not configured.
 * CAA_CURL_DEBUG: Output curl verbose messaged.
 * CAA_TRUST_CA: Add additional CA-bundle to trust.
 * CAA_BASIC_AUTH: Basic authentication in format
 *   username:password  .
 */
define('CAA_API_KEY', '1234');
define('CAA_SSL_V', 0);
define('CAA_CURL_DEBUG', TRUE);
define('CAA_TRUST_CA', '');
define('CAA_BASIC_AUTH', '');

/**
 * Constants, not to be touched.
 */
define('CAA_TMP', '.caa');
define('CAA_TTL', 300);

// Do nothing if not called from command line.
if (php_sapi_name() != 'cli') {
  exit(1);
}

// Parse parameters.
$api = '/api/external_auth_backend';
$drupal = $argv[1];
$endpoint = $drupal . $api;

// Read username / password from stdin.
$username = trim(fgets(STDIN));
$password = trim(fgets(STDIN));

// Prepare POST-data array.
$data = array(
  'api-key' => CAA_API_KEY,
  'username' => $username,
  'password' => $password,
);

// First see if session is already available.
if (caa_get_cached($data)) {
  caa_shutdown($data, 0);
}

// If no session is available, ask auth backend for it.
$result = caa_post_request($endpoint, $data);
if ($result && is_string($result) && $result == 'EXTERNAL-AUTH-AUTHENTICATED') {
  caa_set_cached($data);
  caa_shutdown($data, 0);
}

// Still nothing?
// User is not authenticated.
caa_shutdown($data, 1);

/**
 * Post data to url.
 *
 * @param string $url
 *   URL string.
 * @param array $data
 *   Data array.
 *
 * @return string|bool
 *   Response body or FALSE in case of failure.
 */
function caa_post_request($url, array $data) {

  $postdata = http_build_query($data);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  // Use SSL version.
  if (CAA_SSL_V > 0) {
    curl_setopt($ch, CURLOPT_SSLVERSION, CAA_SSL_V);
  }

  // Trust CA if any.
  $ca = CAA_TRUST_CA;
  if (!empty($ca)) {
    curl_setopt($ch, CURLOPT_CAINFO, CAA_TRUST_CA);
  }

  // Curl debug.
  if (CAA_CURL_DEBUG) {
    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
  }

  // Basic auth.
  $auth = CAA_BASIC_AUTH;
  if (!empty($auth)) {
    curl_setopt($ch, CURLOPT_USERPWD, $auth);
  }

  $result = curl_exec($ch);
  curl_close($ch);

  return $result;
}

/**
 * Store session file.
 *
 * @param array $data
 *   Data array.
 */
function caa_set_cached(array $data) {
  $temp_folder = caa_check_temp_folder($data);
  $hash = md5(http_build_query($data));
  touch($temp_folder . '/' . $hash);
}

/**
 * Get cached session, based on username, password and api-key.
 *
 * @param array $data
 *   Data array.
 *
 * @return bool
 *   TRUE on valid session, FALSE on expired or no-existing one.
 */
function caa_get_cached(array $data) {
  $temp_folder = caa_check_temp_folder($data);

  // Generate hash based on user data, based on provided password etc.
  $hash = md5(http_build_query($data));

  // Look for session file,
  // and if it's still in the TimeToLive period.
  $session_file = $temp_folder . '/' . $hash;
  if (file_exists($session_file)) {
    $modified = filemtime($session_file);
    if ($modified && $modified > (time() - CAA_TTL)) {
      // Still valid.
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Check temp folder and create, based on username, password and api-key.
 *
 * @param array $data
 *   Data array.
 *
 * @return string
 *   Persistent session folder.
 */
function caa_check_temp_folder(array $data) {
  // Check / create temp folder, make it impossible to find
  // by using hash based on api-key.
  $temp_folder = sys_get_temp_dir() . '/' . CAA_TMP . '_' . md5($data['api-key']);
  if (!is_dir($temp_folder)) {
    if (!mkdir($temp_folder, 0700)) {
      throw new Exception('Unable to create temp folder ' . $temp_folder);
    }
  }

  return $temp_folder;
}

/**
 * Clean up old session files.
 *
 * @param array $data
 *   Data array.
 */
function caa_cleanup_sessions(array $data) {
  $temp_folder = caa_check_temp_folder($data);

  if ($handle = opendir($temp_folder)) {
    while (($file = readdir($handle)) !== FALSE) {
      if ($file == '.' || $file == '..' || is_dir($temp_folder . '/' . $file)) {
        continue;
      }

      if ((time() - filemtime($temp_folder . '/' . $file)) > CAA_TTL) {
        unlink($temp_folder . '/' . $file);
      }
    }
    closedir($handle);
  }
}

/**
 * Clean up and exit.
 *
 * @param array $data
 *   Data array.
 * @param int $status
 *   Status code to exit with.
 */
function caa_shutdown(array $data, $status) {
  caa_cleanup_sessions($data);
  exit($status);
}
